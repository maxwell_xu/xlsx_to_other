﻿var XLSX = require('xlsx');
var fs = require('fs');
var path = require('path');

var xlsx2txt = function(fromDir,toDir) {
	var files = fs.readdirSync(fromDir);
	files.forEach(function(fileName){
		if(path.extname(fileName) != '.xlsx') return;
		if(path.basename(fileName)[0] == '~') return;
		var fromPath = path.join(path.resolve(fromDir),fileName);
		var toPath = path.join(path.resolve(toDir),path.basename(fileName,'.xlsx'))+'.txt';
		var workbook = XLSX.readFile(fromPath);
		fs.writeFileSync(toPath,XLSX.utils.sheet_to_csv(workbook.Sheets[workbook.SheetNames[0]],{RS:'\r\n'}));
		console.log('完成',toPath);
	});
}
xlsx2txt('xlsx','txt');