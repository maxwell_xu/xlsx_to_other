namespace ns {
	export namespace cfg {
		export class ActivitySwitch {
			Id: number;		//#表ID序
			ActivityType: number;		//活动类型
			ActivityId: number;		//活动id
			OpenType: number;		//开启类型
			OpenTime: number;		//开启时间
			CloseTime: number;		//持续时间
			RewardTime: number;		//奖励时间
			IntervalTime: number;		//间隔时间
			Desc: string[];		//描述
			BtnIcon: string;		//按钮
			AcTypeBg: string;		//背景
			IsMain: number;		//主界面是否显示
			MainIcon: string;		//主界面图标
			RuleId: number;		//规则Id
		}
	}
}