namespace ns {
	export namespace cfg {
		export class item {
			Id: number;		//#配置ID
			Name: string;		//名称
			Type: number;		//物品大类型
			ItemType: number;		//物品小类型
			Profession: number;		//需要职业
			Level: number;		//需要转职数
			RoleLevel: number;		//需要人物等级
			Quality: number;		//品质
			AmountLimit: number;		//叠加物品数量
			Price: number;		//出售价格
			Icon: string;		//物品图标
			Description: string;		//物品说明
			DropIcon: string;		//掉落图标
			AttrId: number;		//属性ID
			UseNeed: string[];		//打开消耗
			UseGain: number;		//打开获得
			RoleRes: number;		//人物资源外观
			RedPieces: string[];		//神装碎片
			JumpUI: string[];		//跳转获取途径
			GoToUI: string[];		//前往
			RandAttr: string[];		//卓越属性
			Elimit: number;		//强化上限
			Ulimit: number;		//升星上限
			CLevel: number;		//物品阶级
			trademoney: number;		//拍卖起始价格
			SkillId: number;		//装备额外的SkillId
		}
	}
}