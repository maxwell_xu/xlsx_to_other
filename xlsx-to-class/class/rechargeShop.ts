namespace ns {
	export namespace cfg {
		export class rechargeShop {
			Id: number;		//#ID
			Money: number;		//金额
			Cash: number;		//钻石
			Name: string;		//名称
			Des: string;		//描述
			Icon: string;		//图示
			Type: number;		//类型
			Operate: number;		//运营方
			ActivityIndex: number;		//活动索引
			CpID: string;		//cp产品ID
		}
	}
}