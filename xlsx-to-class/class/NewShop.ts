namespace ns {
	export namespace cfg {
		export class NewShop {
			Id: number;		//#ID
			Type: number;		//标签类型
			LimitType: number;		//限购类型
			Limit: number;		//限购次数
			Add: number;		//递增
			Discount: number;		//折扣
			Item: string[];		//物品
			Cost: string[];		//消耗
			GangLv: number;		//家族解锁等级
		}
	}
}