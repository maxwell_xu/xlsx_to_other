namespace ns {
	export namespace cfg {
		export class notic {
			Id: number;		//#ID
			Type: number;		//公告类型
			YearStart: number;		//公告开始年份
			Opentime: string;		//公告开始时间
			YearEnd: number;		//公告结束年份
			Endtime: string;		//公告结束时间
			Content: string;		//公告内容
			Title: string;		//公告标题
			Image: number;		//公告图片
			Activity: number;		//连接活动
		}
	}
}